Millionaire
-----------
Game based on "Who Wants to Be a Millionaire?" TV show.
It gives you opportunity to play it in your living room.
Printing "money-like" paper checks and proper moderator role-playing can provide better gaming experience:-)

Features
--------
* GUI that that allows you to go through the game with cut sounds from original TV show
* Customizable game flow (single player or hotseat)
  * Automatic or manual audience hint voting
  * Automatic or manual phone friend
* GUI for creating your own game files (simple XML)

Running
-------
* Get a release
  * Build your own via `mvn clean install -P release`
  * Download pre-built release from [releases page](https://github.com/d1x/millionaire/releases).
* Extract the ZIP file
* Run the game or game creator from /bin directory (there is one example game in the release)



